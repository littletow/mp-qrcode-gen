# 小工具

该项目采用 Rust 语言，使用 native-windows-gui 开发的小工具。
用于生成微信小程序码。


截图如下：
![二维码生成小工具](https://gitee.com/littletow/mp-qrcode-gen/raw/master/images/mqg1.png)
